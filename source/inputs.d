module inputs;

import derelict.glfw3.glfw3;
import std.experimental.logger;

struct Key {
	bool pressed;
	bool toggled;

	void opAssign(bool val){
		// sets toggled to true if there is a change of pressed state
		if(val != pressed) {
			toggled = true;
		} else {
			toggled = false;
		}

		pressed = val;
	}

	bool opCast() {
		return pressed;
	}
}

struct Control {
	float[12] vector;

	float movex() { return vector[0]; }
	float movex(float val) { return vector[0] = val; }

	float movey() { return vector[1]; }
	float movey(float val) { return vector[1] = val; }
	
	float movez() { return vector[2]; }
	float movez(float val) { return vector[2] = val; }
	
	float rotx() { return vector[3]; }
	float rotx(float val) { return vector[3] = val; }
	
	float roty() { return vector[4]; }
	float roty(float val) { return vector[4] = val; }
	
	float rotz() { return vector[5]; }
	float rotz(float val) { return vector[5] = val; }
}

/**
 * Inputs
 * 
 * gets keypresses and inputs
 */
class Inputs {

	GLFWwindow* window;

	// actions
	Control control;
	float lookUp,  lookDown, lookLeft, lookRight;

	// Keys
	Key A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z;
	Key arrowUp, arrowDown, arrowLeft, arrowRight;
	Key escape;

	this(GLFWwindow* window) {
		this.window = window;
	}

	void update(GLFWwindow* window){
		setKeyState(window);
		setActions();	
	}


	// http://www.glfw.org/docs/latest/group__keys.html
	void setKeyState(GLFWwindow* window) {
		escape = (GLFW_PRESS == glfwGetKey(window, GLFW_KEY_ESCAPE));

		arrowUp = (GLFW_PRESS == glfwGetKey(window, GLFW_KEY_UP));
		arrowDown = (GLFW_PRESS == glfwGetKey(window, GLFW_KEY_DOWN));
		arrowLeft = (GLFW_PRESS == glfwGetKey(window, GLFW_KEY_LEFT));
		arrowRight = (GLFW_PRESS == glfwGetKey(window, GLFW_KEY_RIGHT));

		A = (GLFW_PRESS == glfwGetKey(window, GLFW_KEY_A));
		D = (GLFW_PRESS == glfwGetKey(window, GLFW_KEY_D));
		S = (GLFW_PRESS == glfwGetKey(window, GLFW_KEY_S));
		L = (GLFW_PRESS == glfwGetKey(window, GLFW_KEY_L));
		W = (GLFW_PRESS == glfwGetKey(window, GLFW_KEY_W));
		Q = (GLFW_PRESS == glfwGetKey(window, GLFW_KEY_Q));
		E = (GLFW_PRESS == glfwGetKey(window, GLFW_KEY_E));
		
	}

	void setActions(){
		foreach(ref n; control.vector) { n = 0; }
		if(W) control.movez = control.movez + 1;
		if(S) control.movez = control.movez - 1;
		if(D) control.movex = control.movex + 1;
		if(A) control.movex = control.movex - 1;
		if(Q) control.movey = control.movey + 1;	
		if(E) control.movey = control.movey - 1;	

		if(arrowDown) 	control.rotx = control.rotx + 1;
		if(arrowUp) 	control.rotx = control.rotx - 1;
		if(arrowRight) 	control.roty = control.roty + 1;
		if(arrowLeft) 	control.roty = control.roty - 1;
	}
}
