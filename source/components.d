module components;

import gl3n.linalg;

public import ode_physics: BodyComponent, GeometryComponent;


enum ComponentType
{
	unknown 	= 0x0,
	model 		= 0x1,
	geom 		= 0x2,
	body 		= 0x4,
	position 	= 0x8,
	velocity 	= 0x16,
	transform 	= 0x32,
}

struct Components
{
	ModelComponent[int] models;
	GeometryComponent[int] geoms;
	BodyComponent[int] bodies;
	PositionComponent[int] positions;
	VelocityComponent[int] velocities;
	TransformComponent[int] transforms;
}

struct ModelComponent 
{
	string name;

	ComponentType type() 
	{
		return ComponentType.model;
	}
}

struct PositionComponent
{
	vec3 position;
	
	ComponentType type() 
	{
		return ComponentType.position;
	}
}

struct VelocityComponent
{
	vec3 velocity;

	ComponentType type() 
	{
		return ComponentType.velocity;
	}
}

struct TransformComponent
{
	vec3 position;
	vec3 rotation;
	vec3 scale;

	ComponentType type() 
	{
		return ComponentType.transform;
	}
}
