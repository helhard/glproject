module util;

import std.math;
import std.traits;
import std.typecons;

import gl3n.linalg;
import gl3n.math;




alias Point = vec2;

struct Circle {
	vec2 center;
	float radius;
}

struct AABB {
	vec2 min;
	vec2 max;
}


struct Line {
	vec2 start;
	vec2 end;
}


enum Shape : int {
	none,
	point,
	circle,
	aabb,
}



bool sameSign(T)(T a, T b) {
	return(signbit(a) == signbit(b));
}

vec2 closestPointOnLine(Line l, vec2 p) {
	// http://ericleong.me/research/circle-circle/
    float A1 = l.end.y - l.start.y;
    float B1 = l.start.x - l.end.x;
    float C1 = (A1) * l.start.x + (B1) * l.start.y;
    float C2 = -B1 * p.x + A1 * p.y;
    float det = A1^^2 - -B1^^2;
    vec2 c;
    if(det != 0) {
        c.x = (A1*C1 - B1*C2)/det;
        c.y = (A1*C2 - -B1*C1)/det;
    } else {
        c.x = p.x;
        c.y = p.y;
    } 
    return c;
}

auto getLineIntersection(Line l1, Line l2) {
	// http://ericleong.me/research/circle-line/
	double A1 = l1.end.y - l1.start.y;
	double B1 = l1.start.x - l1.end.x;
	double C1 = A1*l1.start.x + B1*l1.start.y;

	double A2 = l2.end.y - l2.start.y;
	double B2 = l2.start.x - l2.end.x;
	double C2 = A2*l2.start.x + B2*l2.start.y;

	double det = A1*B2 - A2*B1;
	if(det != 0) {
		double x = (B2*C1 - B1*C2)/det;
		double y = (A1*C2 - A2*C1)/det;
		if(x >= min(l1.start.x, l1.end.x) && x <= max(l1.start.x, l1.end.x)
			&& x >= min(l2.start.x, l2.end.x) && x <= max(l2.start.x, l2.end.x)
			&& y >= min(l1.start.y, l1.end.y) && y <= max(l1.start.y, l1.end.y)
			&& y >= min(l2.start.y, l2.end.y) && y <= max(l2.start.y, l2.end.y))
		{
			return tuple(true, vec2(x, y));
		}
	}
	return tuple(false, vec2.init);
}
