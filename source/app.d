import std.experimental.logger;

import game_manager;
import inputs;
import model;
import settings;
import window_manager;


void main() {
	auto settings = new Settings("settings.sdl");
	auto window = new WindowManager(settings.fbWidth, settings.fbHeight, settings.vsync,
			settings.fullscreen, "GLG");

	// making game manager needs to come after creating an opengl context (the window)
	auto gm = new GameManager();
	auto inputs = new Inputs(window.window);

	//assetManager.loadLevel(gm, "zero");


	/// Main loop!
	while(!window.shouldClose()) {
		inputs.update(window.window);
		if(inputs.escape){
			window.close();
			break;
		}

		gm.update(1.0/60, window, inputs);
		window.update();
	}
}
