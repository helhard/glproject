module settings;

import sdlang;
import std.experimental.logger;

class Settings {
	// frame buffer
	const int fbWidth;
	const int fbHeight;

	const bool vsync;
	const bool fullscreen;

	this(string settingsPath){
		auto root = new Tag;
		try
			root = parseFile(settingsPath);
		catch(Exception e)
			error("Error parsing settings file");

		auto window = root.getTag("window", new Tag);
		fbWidth = window.getTagValue!int("FramebufferWidth", 800);
		fbHeight = window.getTagValue!int("FramebufferHeight", 480);
		vsync = window.getTagValue!bool("vsync", true);
		fullscreen = window.getTagValue!bool("fullscreen", false);
	}
}
