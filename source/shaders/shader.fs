#version 120

varying vec2 Texcoord;

// out vec4 outColor;

uniform sampler2D texture_diffuse1;
uniform sampler2D texture_diffuse2;
uniform sampler2D texture_diffuse3;
uniform sampler2D texture_diffuse4;

uniform sampler2D texture_specular1;
uniform sampler2D texture_specular2;
uniform sampler2D texture_specular3;
uniform sampler2D texture_specular4;
// uniform float time;

void main() {
	gl_FragColor = texture2D(texture_diffuse1, Texcoord);
	// gl_FragColor = vec4(0.0, 1.0, 0.0, 1.0);
}
