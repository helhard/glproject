#version 120

varying vec2 Texcoord;

// out vec4 outColor;

uniform sampler2D tex;
// uniform float time;

void main() {
	gl_FragColor = texture2D(tex, Texcoord);
}
