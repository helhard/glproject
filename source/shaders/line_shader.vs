#version 120

attribute vec3 position;
attribute vec3 color;

varying vec3 Color;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main() {
    Color = color;
    gl_Position = projection * view * model * vec4(position, 1.0);
}
