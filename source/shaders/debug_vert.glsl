#version 120

attribute vec2 position;

uniform mat4 view;
uniform mat4 proj;
// uniform vec3 overridecolor;

void main() {
    gl_Position = proj * view * vec4(position, 0.0, 1.0);
}
