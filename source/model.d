//module model;

//import std.container: SList;
//import std.conv: to;
//import std.experimental.logger;
//import std.path;
//import std.string;

//import derelict.assimp3.assimp;
//import draw_system;
//import material;


//struct ModelComponent {
//	string key;
//	string name;
	
//	string path;
//	Mesh[] meshes;

//	/*
//		static constructor runs before we enter main function
//		runs once to load the shared lib but not every time 
//		I make a new instance of ModelComponent
//	*/
//	static this() {
//		DerelictASSIMP3.load();
//	}

//	this(string modelPath) {
//		meshes = loadMeshes(modelPath);
//	}


//	/*
//		Loads a 3d model file using assimp and returns an array of all
//		the meshes it contains
//	*/
//	Mesh[] loadMeshes(string modelPath) {
//		auto scene = aiImportFile(modelPath.toStringz(),
//			aiProcess_Triangulate | aiProcess_GenNormals | aiProcess_FlipUVs);
//		if(scene is null) {
//			throw new Exception("Could not load scene from: " ~ modelPath);
//		}

//		Mesh[] meshes;
//		// use a stack to recursively load meshes from all childnodes
//		auto stack = SList!(aiNode*)();
//		stack.insert(cast(aiNode*) scene.mRootNode);
//		while(!stack.empty) {
//			auto node = stack.front;
//			stack.removeFront;

//			foreach(i; 0 .. node.mNumMeshes) {
//				auto aiMesh = scene.mMeshes[node.mMeshes[i]];
//				auto mesh = processMesh(aiMesh, scene);
//				meshes ~= mesh;
//			}

//			foreach(i; 0 .. node.mNumChildren) {
//				stack.insert(node.mChildren[i]);
//			}
//		}

//		return meshes;
//	}

//	/*
//		Generates and returns a Mesh from the vertex data in the aiMesh struct
//	*/
//	Mesh processMesh(const aiMesh* mesh, const aiScene* scene){
//		Vertex[] vertices;
//		uint[] indices;
//		Texture[] textures;
//		auto name = to!string(mesh.mName.data[0 .. mesh.mName.length]);

//		foreach(i; 0 .. mesh.mNumVertices) {
//			Vertex vertex;
//			vertex.position.x = mesh.mVertices[i].x;
//			vertex.position.y = mesh.mVertices[i].y;
//			vertex.position.z = mesh.mVertices[i].z;

//			vertex.normal.x = mesh.mNormals[i].x;
//			vertex.normal.y = mesh.mNormals[i].y;
//			vertex.normal.z = mesh.mNormals[i].z;

//			// [0] because we just want the first set of tex coords
//			vertex.uv.x = mesh.mTextureCoords[0][i].x;
//			vertex.uv.y = mesh.mTextureCoords[0][i].y;
			
//			vertices ~= vertex;
//		}

//		foreach(i; 0 .. mesh.mNumFaces) {
//			const aiFace face = mesh.mFaces[i];
//			foreach(k; 0 .. face.mNumIndices) {
//				indices ~= face.mIndices[k];
//			}
//		}

//		const aiMaterial* material = scene.mMaterials[mesh.mMaterialIndex];
//		textures ~= getTextures(material, aiTextureType_DIFFUSE);
//		textures ~= getTextures(material, aiTextureType_SPECULAR);

//		return Mesh(name, vertices, indices, textures);
//	}

//	/*
//		Returns an array of all the textures of the specified type found in the 
//		specified material
//	*/
//	Texture[] getTextures(const aiMaterial* material, aiTextureType type) {
//		Texture[] textures;
//		uint textureCount = aiGetMaterialTextureCount(material, type);
//		foreach(i; 0 .. textureCount) {
//			aiString path_cstr;
//			aiGetMaterialTexture(material, type, i, &path_cstr,
//				null, null, null, null, null, null);

//			string name;
//			if(type == aiTextureType_DIFFUSE) {
//				name = "texture_diffuse" ~ to!string(i);
//			} else if(type == aiTextureType_SPECULAR) {
//				name = "texture_specular" ~ to!string(i);
//			}

//			auto texturePath = to!string(fromStringz(&path_cstr.data[0]));
//			auto texture = Texture(name, texturePath);
//			textures ~= texture;
//		}
//		return textures;
//	}
//}
