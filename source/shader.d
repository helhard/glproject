module shader;

import derelict.opengl;
import std.conv;
import std.file;
import gl3n.linalg;
import std.string;
import core.vararg;

struct Attribute {
	int index;
	string name;
}

/// Shader program wrapper
class Shader {
	GLuint program;

	this(string vpath, string fpath, Attribute[] attr ...) {
		import std.string;
		auto vertex = readText(vpath).toStringz;
		auto fragment = readText(fpath).toStringz;

		GLuint vsID = glCreateShader(GL_VERTEX_SHADER);
		GLchar* vs = cast(char*) &vertex[0];
		glShaderSource(vsID, 1, &vs, null);
		glCompileShader(vsID);
		checkShaderError(vsID, vpath);

		GLuint fsID = glCreateShader(GL_FRAGMENT_SHADER);
		GLchar* fs = cast(char*) &fragment[0];
		glShaderSource(fsID, 1, &fs, null);
		glCompileShader(fsID);
		checkShaderError(fsID, fpath);

		program = glCreateProgram();

		foreach(Attribute attribute; attr) {
			glBindAttribLocation(program, attribute.index, attribute.name.toStringz());
		}

		glAttachShader(program, vsID);
		glAttachShader(program, fsID);

		glLinkProgram(program);

		// mark shaders for deletion
		// won't be deleted while attached to a program object
		glDeleteShader(vsID);
		glDeleteShader(fsID);

		checkGLError("Error generating shader program");
	}

	~this() {
		// marks program for deletion
		glDeleteProgram(program);
	}

	void use() {
		glUseProgram(program);
	}

	void setUniformMat4(string name, mat4 val, bool transpose = true) {
		GLint uniform = glGetUniformLocation(this.program, toStringz(name));
		GLboolean tpose = cast(GLboolean) transpose;
		glUniformMatrix4fv(uniform, 1, tpose, val.value_ptr);
	}

	void setUniformInt(string name, int val) {
		GLint uniform = glGetUniformLocation(this.program, toStringz(name));
		glUniform1i(uniform, val); 
	}

	void bindTexture(string name, int val, GLuint textureId) {

	}

	private void checkShaderError(GLuint shader, string filePath) {
		GLint status;
		glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
		if(status == GL_FALSE){
			GLint maxLength = 0;
			glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &maxLength);
			// The maxLength includes the NULL character
			GLchar[] errorLog;
			errorLog.length = maxLength;
			glGetShaderInfoLog(shader, maxLength, &maxLength, errorLog.ptr);
			throw new Exception(
				"couldn't compile shaders. \n"
				~ filePath
				~ "\nError: "
				~ cast(string)errorLog
			);
		}
		checkGLError("Error compiling shader");
	}
}



/**
*	Check for OpenGL errors
*/
void checkGLError(string message = "", int l = __LINE__) {
	import std.stdio;
	auto errorCode = glGetError();
	string error;
	bool foundErr = false;
	while(errorCode != GL_NO_ERROR) {
		foundErr = true;
		switch(errorCode) {
			case GL_INVALID_ENUM:			error="GL_INVALID_ENUM";			break;
			case GL_INVALID_VALUE:			error="GL_INVALID_VALUE";			break;
			case GL_INVALID_OPERATION:		error="GL_INVALID_OPERATION";		break;
			case GL_INVALID_FRAMEBUFFER_OPERATION:	error="GL_INVALID_FRAMEBUFFER_OPERATION";	break;
			case GL_OUT_OF_MEMORY:			error="GL_OUT_OF_MEMORY";			break;
			//case GL_STACK_UNDERFLOW:		error="GL_STACK_UNDERFLOW";			break;
			//case GL_STACK_OVERFLOW:			error="GL_STACK_OVERFLOW ";			break;
			default: break;
		}
		writeln("GL Error at line: ", l, "): ", error);
		writeln(message);
		errorCode = glGetError();
	}
	if(foundErr) throw new Exception("Error at: " ~ to!string(l));
}

