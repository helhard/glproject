module material;

import gl3n.linalg;
import gl3n.math;
import derelict.opengl : GLuint;
import imageformats;
import shader: checkGLError;
import std.experimental.logger;


struct Material
{
	string name;
	Texture albedo;
	Texture normal;
	Texture metalness;
	Texture roughness;
	Texture ao;
}

enum TextureType
{
	albedo,
	normal,
	metalness,
	roughness,
	ao,
}

struct Texture
{
	IFImage image;
	GLuint id;

	this(string path)
	{
		int w, h, c;
		read_image_info(path, w, h, c);
		// we want max 3 channels, RGB, not RGBA
		this.image = read_image(path, min(3, c));
	}
}
