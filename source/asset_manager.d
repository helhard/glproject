//module asset_manager;

//import std.path;
//import std.file;
//import std.experimental.logger;

//import camera;
//import game_manager;
//import sdlang;
//import components;
//import gl3n.linalg;

//enum dataFolder = "data" ~ dirSeparator;
//enum modelFolder = dataFolder ~ "models" ~ dirSeparator;
//enum textureFolder = dataFolder ~ "textures" ~ dirSeparator;

//class AssetManager {
	
//	string[string] modelPaths;
//	Tag[string] levels;

//	this() {
//		getModelPaths(dataFolder ~ "models.sdl");
//		setupLevels(dataFolder ~ "levels.sdl");
//	}

//	void loadLevel(GameManager gm, string levelName) {
//		Tag* level = (levelName in levels);
//		assert(level !is null, "Trying to load a level that doesn't exist");

//		auto start = level.expectTag("start");
//		addCamera(gm, start);

//		auto entities = level.maybe.tags["entity"];
//		foreach(entity; entities) {
//			int id = gm.createEntity();
//			string[] tagNames = [
//				"model",
//				"position",
//			];

//			foreach(tagname; tagNames) {
//				auto componentTag = entity.getTag(tagname);
//				switch(tagname) {
//					default: break;
//					case "model":
//						string modelID = componentTag.getValue!string();
//						string modelPath = modelFolder ~ modelPaths[modelID];
//						auto model = ModelComponent(modelPath);
//						gm.addComponent(id, model);
//						break;
//					case "position":
//						auto values = componentTag.values;
//						vec3 v = vec3(
//							values[0].get!float,
//							values[1].get!float,
//							values[2].get!float
//						);
//						auto position = PositionComponent(v);
//						gm.addComponent(id, position);
//						break;
//				}
//			}
//		}
//	}

//	void addCamera(GameManager gm, Tag start) {
//		auto posValues = start.expectTag("position").values;
//		auto position = vec3(
//			posValues[0].get!float,
//			posValues[1].get!float,
//			posValues[2].get!float
//		);
		
//		auto dirValues = start.expectTag("direction").values;
//		auto direction = vec3(
//			dirValues[0].get!float,
//			dirValues[1].get!float,
//			dirValues[2].get!float
//		);
		
//		gm.camera = new FPSCamera(position, direction);
//	}

//	void getModelPaths(string modelsFilePath) {
//		Tag modelRoot = parseFile(modelsFilePath);
//		auto modelTags = modelRoot.maybe.tags["model"];
//		foreach(model; modelTags) {
//			// name/id as key and path as value
//			modelPaths[model.getValue!string()] = model.values[1].get!string;
//		} 
//	}

//	void setupLevels(string levelsPath) {
//		Tag levelsRoot = parseFile(levelsPath);
//		auto levelsTag = levelsRoot.maybe.tags["level"];
//		foreach(level; levelsTag) {
//			levels[level.getValue!string()] = level;
//		}
//	}
//}