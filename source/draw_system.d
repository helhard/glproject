module draw_system;

import camera;
import derelict.opengl;
import gl3n.linalg;
import imageformats;
import shader;
import std.conv: to;
import std.experimental.logger;
import std.string;
import window_manager;
import material;


struct Vertex
{
	vec3 position;
	vec3 normal;
	vec2 uv;
}

struct Mesh
{
	string name;
	Vertex[] vertices;
	GLuint[] indices;
	string material;
	private GLuint VAO, VBO, EBO;
	bool visible;
}

struct Model
{
	string name;
	Mesh[] meshes;
}


class DrawSystem
{
	Shader shader;
	Shader lineShader;
	Material[string] materials;
	Model[string] models;

	this()
	{
		DerelictGL3.load();
		auto loaded = DerelictGL3.reload();
		logf("OpenGL version loaded: %s", loaded);
		if (loaded < GLVersion.gl21)
		{
			throw new Exception("Opengl version to low");
		}

		shader = new Shader(
			"source/shaders/shader.vs",
			"source/shaders/shader.fs",
			Attribute(0, "position"),
			Attribute(1, "normal"),
			Attribute(2, "texcoord"),
		);

		lineShader = new Shader(
			"source/shaders/line_shader.vs",
			"source/shaders/line_shader.fs",
			Attribute(0, "position"),
			Attribute(1, "color"),
		);
	}

	void addModel(Model model)
	{
		Model* p;
		p = (model.name in models);
		if (p is null)
		{
			loadModelToGPU(model);
			models[model.name] = model;
		}
	}

	private void loadModelToGPU(Model model)
	{
		foreach (mesh; model.meshes)
		{
			glGenVertexArrays(1, &mesh.VAO);
			glBindVertexArray(mesh.VAO);

			glGenBuffers(1, &mesh.VBO);
			glGenBuffers(1, &mesh.EBO);

			glBindBuffer(GL_ARRAY_BUFFER, mesh.VBO);
			glBufferData(
				GL_ARRAY_BUFFER,
				mesh.vertices.length * Vertex.sizeof,
				mesh.vertices.ptr,
				GL_STATIC_DRAW
			);

			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh.EBO);
			glBufferData(
				GL_ELEMENT_ARRAY_BUFFER,
				mesh.indices.length * GLuint.sizeof,
				mesh.indices.ptr,
				GL_STATIC_DRAW
			);

			// vertices to attrib array 0
			glEnableVertexAttribArray(0);
			glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, Vertex.sizeof, null);

			// normals to attrib array 1
			glEnableVertexAttribArray(1);
			glVertexAttribPointer(
				1,
				3,
				GL_FLOAT,
				GL_FALSE,
				Vertex.sizeof,
				cast(void*)Vertex.normal.offsetof
			);

			// uv to attrib array 2
			glEnableVertexAttribArray(2);
			glVertexAttribPointer(
				2,
				2,
				GL_FLOAT,
				GL_FALSE,
				Vertex.sizeof,
				cast(void*)Vertex.uv.offsetof
			);

			checkGLError("mesh.setup");
			glBindVertexArray(0);
		}
	}

	void addMaterial(Material material)
	{
		Material* p;
		p = (material.name in materials);
		if (p is null)
		{
			material.albedo.id = loadTextureToGPU(material.albedo.image);
			material.normal.id = loadTextureToGPU(material.normal.image);
			material.metalness.id = loadTextureToGPU(material.metalness.image);
			material.roughness.id = loadTextureToGPU(material.roughness.image);
			material.ao.id = loadTextureToGPU(material.ao.image);
			materials[material.name] = material;
		}
	}

	private GLuint loadTextureToGPU(IFImage image)
	{

		GLuint textureId;
		glGenTextures(1, &textureId);
		glActiveTexture(GL_TEXTURE0);

		// make this texture id active so we can work on it
		glBindTexture(GL_TEXTURE_2D, textureId);

		if (image.c == ColFmt.RGB)
		{	
			glTexImage2D(
				GL_TEXTURE_2D,
				0, // lod level
				GL_RGB, 
				image.w, 
				image.h, 
				0, // must be 0
				GL_RGB, 
				GL_UNSIGNED_BYTE, 
				&image.pixels[0]
			);
		}
		else
		{
			glTexImage2D(
				GL_TEXTURE_2D,
				0, // lod level
				GL_RED, 
				image.w, 
				image.h, 
				0, // must be 0
				GL_RED, 
				GL_UNSIGNED_BYTE, 
				&image.pixels[0]
			);
		}
		// set the parameters 
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		// unbind
		glBindTexture(GL_TEXTURE_2D, 0);
		checkGLError("Error loading texture");
		return textureId;
	}

	void draw(mat4 viewMatrix, mat4 projectionMatrix)
	{
		glEnable(GL_DEPTH_TEST);
		
		glClearColor(1, 1, 1, 1);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		drawWorldLines(viewMatrix, projectionMatrix);
	}

	void drawMeshes(Mesh[] meshes, vec3 position, mat4 viewMatrix,
		mat4 projectionMatrix)
	{
		auto modelMatrix = mat4.identity;
		modelMatrix.translate(position);
		shader.use();
		shader.setUniformMat4("model", modelMatrix);
		shader.setUniformMat4("view", viewMatrix);
		shader.setUniformMat4("projection", projectionMatrix);

		foreach (mesh; meshes)
		{
			//foreach (i, texture; mesh.textures)
			//{
			//	shader.setUniformInt(texture.name, to!int(i));
			//	glActiveTexture(GL_TEXTURE0 + to!uint(i));
			//	glBindTexture(GL_TEXTURE_2D, texture.id);
			//}

			glBindVertexArray(mesh.VAO);
			glBindBuffer(GL_ARRAY_BUFFER, mesh.VBO);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh.EBO);

			int length = to!int(mesh.indices.length);
			glDrawElements(GL_TRIANGLES, length, GL_UNSIGNED_INT, null);
			glBindVertexArray(0);
			glActiveTexture(GL_TEXTURE0);
		}
	}

	void drawWorldLines(mat4 view, mat4 projection)
	{
		mat4 model = mat4.identity();
		float[42] lines = [
			0, 0, 0, 	1, 0, 0,
			1, 0, 0, 	1, 0, 0,
			0, 0, 0, 	0, 1, 0,
			0, 1, 0, 	0, 1, 0,
			0, 0, 0, 	0, 0, 1,
			0, 0, 1, 	0, 0, 1,

			0, 0, 0,    0, 0, 0,
		];
		GLuint VBO, VAO;
		glGenVertexArrays(1, &VAO);
		glBindVertexArray(VAO);

		glGenBuffers(1, &VBO);
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferData(GL_ARRAY_BUFFER, lines.sizeof, &lines, GL_STATIC_DRAW);

		// positions to attrib array 0
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * float.sizeof, null);

		// colors to attrib array 1
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 3 * float.sizeof,
			cast(void*)(float.sizeof * 3));

		lineShader.use();
		lineShader.setUniformMat4("view", view);
		lineShader.setUniformMat4("model", model);
		lineShader.setUniformMat4("projection", projection);

		glDrawArrays(GL_LINES, 0, 36);
		//glDrawArrays(GL_POINTS, 36, 1);

		checkGLError("lines");

		glBindVertexArray(0);
	}
}
