module window_manager;

import std.string;
import derelict.glfw3.glfw3;

struct FrameBuffer {
	int height;
	int width;
}

class WindowManager {
	GLFWwindow* window;
	int width, height;
	const string name;


	this(int width, int heigth, bool vsync, bool fullscreen, string name) {
		this.name = name;

		DerelictGLFW3.load();
		if (!glfwInit()) {
			throw new Exception("glfwInit didn't initialize");
		}

		window = glfwCreateWindow(
			width, // window width
			heigth, // window height
			toStringz(name),
			null, // monitor if fullscreen
			null, // window to share context with
		);

		glfwMakeContextCurrent(window);
		glfwSwapInterval(cast(int) vsync); 
		updateFBSize();
	}

	~this(){
		glfwDestroyWindow(window);
		glfwTerminate();
	}


	bool shouldClose(){
		return cast(bool) glfwWindowShouldClose(window);
	}

	void close(){
		glfwSetWindowShouldClose(window, true);
	}

	void update() {
		glfwSwapBuffers(window);
		glfwPollEvents();
		updateFBSize();
	}

	private void updateFBSize() {
		glfwGetFramebufferSize(window, &width, &height);
	}
}