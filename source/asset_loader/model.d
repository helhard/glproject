module asset_loader.model;

import asset_loader;

import components;
import derelict.assimp3.assimp;
import draw_system;
import gl3n.linalg;
import std.container: SList;
import std.conv : to;
import std.experimental.logger;
import std.file;
import std.json;
import std.path;
import std.string;
import std.variant;


class ModelLoader : ITypeLoader
{
	immutable string modelFolder = dataFolder ~ "models" ~ dirSeparator;
	immutable string modelsFileName = modelFolder ~ "models.json";
	string[string] modelPaths;

	this()
	{
		DerelictASSIMP3.load();
		string jsonText = readText(modelsFileName);
		JSONValue parsedJSON = parseJSON(jsonText);
		auto models = parsedJSON["models"].array;
		foreach (model; models)
		{
			modelPaths[model["name"].str] = model["path"].str;
		}
	}

	Variant load(string name)
	{	
		string path = modelPaths[name];
		Variant a = getModel(name, path);
		return a;
	}

	/*
		Loads a 3d model file and returns an array of all
		the meshes it contains in a Model struct
	*/
	private Model getModel(string name, string modelPath)
	{
		string path = modelFolder ~ modelPath;
		auto scene = aiImportFile(path.toStringz(),
			aiProcess_Triangulate | aiProcess_GenNormals | aiProcess_FlipUVs);
		if (scene is null)
		{
			throw new Exception("Could not load scene from: " ~ modelPath);
		}

		Mesh[] meshes;
		// use a stack to recursively load meshes from all childnodes
		auto stack = SList!(aiNode*)();
		stack.insert(cast(aiNode*) scene.mRootNode);
		while (!stack.empty)
		{
			auto node = stack.front;
			stack.removeFront;

			foreach (i; 0 .. node.mNumMeshes)
			{
				auto aiMesh = scene.mMeshes[node.mMeshes[i]];
				auto mesh = processMesh(aiMesh, scene); 
				meshes ~= mesh;
			}

			foreach (i; 0 .. node.mNumChildren)
			{
				stack.insert(node.mChildren[i]);
			}
		}
		return Model(name, meshes);
	}

	/*
		Generates and returns a Mesh from the vertex data in the aiMesh struct
	*/
	private Mesh processMesh(const aiMesh* mesh, const aiScene* scene)
	{
		Vertex[] vertices;
		foreach (i; 0 .. mesh.mNumVertices)
		{
			Vertex vertex;
			vertex.position.x = mesh.mVertices[i].x;
			vertex.position.y = mesh.mVertices[i].y;
			vertex.position.z = mesh.mVertices[i].z;

			vertex.normal.x = mesh.mNormals[i].x;
			vertex.normal.y = mesh.mNormals[i].y;
			vertex.normal.z = mesh.mNormals[i].z;

			// [0] because we just want the first set of tex coords
			vertex.uv.x = mesh.mTextureCoords[0][i].x;
			vertex.uv.y = mesh.mTextureCoords[0][i].y;
			
			vertices ~= vertex;
		}

		uint[] indices;
		foreach (i; 0 .. mesh.mNumFaces)
		{
			const aiFace face = mesh.mFaces[i];
			foreach (k; 0 .. face.mNumIndices)
			{
				indices ~= face.mIndices[k];
			}
		}

		const aiMaterial* material = scene.mMaterials[mesh.mMaterialIndex];
		aiString name;
		aiGetMaterialString(material, "?mat.name", 0, 0, &name);
		string materialID = to!string(name.data[0 .. name.length]);
		auto meshID = to!string(mesh.mName.data[0 .. mesh.mName.length]);

		return Mesh(meshID, vertices, indices, materialID);
	}
}
