module asset_loader.material;

import asset_loader : ITypeLoader, dataFolder;

import gl3n.linalg;
import material;
import std.conv ;
import std.experimental.logger;
import std.file;
import std.path;
import std.traits;
import std.variant;

/**
 * MaterialLoader
 */
class MaterialLoader : ITypeLoader
{
	enum materialFolder = dataFolder ~ "materials" ~ dirSeparator;

	Variant load(string matName)
	{	
		Variant material = getMaterial(matName);
		return material;
	}

	private Material getMaterial(string matName)
	{
		Material material;
		string materialPath = materialFolder ~ matName ~ dirSeparator;
		if(materialPath.exists)
		{
			material.name = matName;
			foreach (textureType; EnumMembers!TextureType)
			{
				immutable string typeName = to!string(textureType);
				string imageFile = materialPath ~ typeName ~ ".png";
				if(imageFile.exists)
				{
					// same as material.<type>Map = Texture(...);
					// like material.albedoMap = ...
					__traits(getMember, material, typeName) = Texture(imageFile);
				}
				else
				{
					errorf("Can't find neither image filefile for " ~
						"%s in material %s", typeName, matName);
				}
			}
		}
		else
		{
			errorf("Can't find material called %s", matName);
			// TODO: load default instead
		}
		return material;
	}
}
