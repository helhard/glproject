module asset_loader;

import asset_loader.material;
import asset_loader.model;

import std.experimental.logger;
import std.file;
import std.path;
import std.variant;
import std.conv;


immutable string dataFolder = "data" ~ dirSeparator;


interface ITypeLoader
{
	Variant load(string);
}


class AssetLoader
{
	ITypeLoader[string] loaders;

	this()
	{
		addLoader(new ModelLoader(), "model");
		addLoader(new MaterialLoader(), "material");
	}

	void addLoader(ITypeLoader loader, string key)
	{
		loaders[key] = loader;
	}

	Variant load(string type, string id)
	{
		return loaders[type].load(id);
	}
}
