module entity_manager;

import gl3n.linalg;
import components;

alias BitMask = uint;

/**
 * EntityManager
 */
struct EntityManager
{	
	private int numberOfEntities;
	int[] entities;

	BitMask[int] masks;
	Components components;

	int createEntity()
	{
		const int id = numberOfEntities++;
		entities ~= id;
		addMask(id, ComponentType.unknown);
		return id;
	}

	bool maskMatches(int id, ComponentType[] types ...)
	{
		BitMask mask;
		foreach (type; types)
		{
			mask |= type;
		}
		return (masks[id] & mask) == mask;
	}

	void addMask(int id, ComponentType type)
	{
		masks[id] |= type;
	}

	void addTransformComponent(int id, TransformComponent tc)
	{
		components.transforms[id] = tc;
		addMask(id, tc.type);
	}

	void addComponent(int id, ModelComponent model)
	{
		components.models[id] = model;
		addMask(id, model.type);
	}

	//void addComponent(int id, GeometryComponent geom)
	//{
	//	//physicsSystem.addGeom(id, geom);
	//	components.geoms[id] = geom;
	//	addMask(id, geom.type);
	//}

	//void addComponent(int id, BodyComponent body)
	//{
	//	//physicsSystem.addBody(id, body);
	//	components.bodies[id] = body;
	//	addMask(id, body.type);
	//}

	void addComponent(int id, PositionComponent position)
	{
		components.positions[id] = position;
		addMask(id, position.type);
	}

	void addComponent(int id, VelocityComponent velocity)
	{
		components.velocities[id] = velocity;
		addMask(id, velocity.type);
	}
}
