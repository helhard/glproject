module ode_physics;

import derelict.ode.ode;
import std.experimental.logger;
import gl3n.linalg;


enum Types {
	Undefined,
	Box,
	Sphere,
}

abstract class Shape {
	const Types type;
	this(Types t){
		this.type = t;
	}
}

class Box : Shape {
	const double width, height, length;
	
	this(double width, double height, double length) {
		super(Types.Box);
		this.width = width;
		this.height = height;
		this.length = length;
	}
}

class Sphere : Shape {
	const double radius;

	this(double radius){
		super(Types.Sphere);
		this.radius = radius;
	}
}

struct GeometryComponent {
	vec3d position;
	Shape shape;
	ushort space;
	private dGeomID geomID;
}

struct BodyComponent {
	vec3d position;
	double mass; // in Kg
	Shape shape;
	bool dynamic;
	private dBodyID bodyID;
}


/**
 * OdePhysics
 */
class OdePhysics {
	dWorldID world;
	dSpaceID[ushort] spaces;
	dBodyID[int] bodies;

	dJointGroupID contactgroup;

	this(){
	    DerelictODE.load();
	    dInitODE2(0); // initalize ODE

	    world = dWorldCreate();
	    dWorldSetGravity(world, 0, -10, 0);
	    spaces[0] = dSimpleSpaceCreate(null);
	    contactgroup = dJointGroupCreate(0);
	}
 

	~this(){
		dCloseODE();
	}

	void addGeom(int id, GeometryComponent geom) {
		switch(geom.shape.type){
			default: 
				assert(0);
			case Types.Box:
				auto box = cast(Box) geom.shape;
				geom.geomID = dCreateBox(spaces[geom.space], box.width, box.height, box.length);
				break;
			case Types.Sphere:
				auto sphere = cast(Sphere) geom.shape;
				geom.geomID = dCreateSphere(spaces[geom.space], sphere.radius);
				break;
		}

		dBodyID* bodyID = (id in bodies);
		if(bodyID !is null) {
			dReal* bodyPosition = dBodyGetPosition(*bodyID);
			geom.position.vector = bodyPosition[0 .. 3].dup;
			dGeomSetBody(geom.geomID, *bodyID);
		}

		dGeomSetPosition(geom.geomID, geom.position.x, geom.position.y, geom.position.z);
	}

	// add bodies before geoms
	void addBody(int id, BodyComponent body) {
		dMass mass;
		switch(body.shape.type){
			default: 
				assert(0);
			case Types.Box:
				auto box = cast(Box) body.shape;
				dMassSetBoxTotal(&mass, body.mass, box.width, box.height, box.length);
				break;
			case Types.Sphere:
				auto sphere = cast(Sphere) body.shape;
				dMassSetSphereTotal(&mass, body.mass, sphere.radius);
				break;
		}

		body.bodyID = dBodyCreate(world);
		dBodySetMass(body.bodyID, &mass);
		dBodySetAutoDisableFlag(body.bodyID, 1);
		dBodySetPosition(body.bodyID, body.position.x, body.position.y, body.position.z);
		
		if(!body.dynamic) {
			dBodySetKinematic(body.bodyID);
		}
		bodies[id] = body.bodyID;
	}

	void update(double dt = 1.0/60){
		foreach(space; spaces){
			dSpaceCollide(space, cast(void*) this, &nearCallback);
		}
		dWorldStep(world, dt);
		dJointGroupEmpty(contactgroup);
	}
}

extern(C) void nearCallback(void* data, dGeomID o1, dGeomID o2) nothrow {
	OdePhysics p = cast(OdePhysics) data;

	dBodyID b1 = dGeomGetBody(o1);
	dBodyID b2 = dGeomGetBody(o2);

	dContact contact;
	int numc = dCollide(o1, o2, 1, &contact.geom, dContact.sizeof);
	if(numc > 0){
		dJointID contactJoint = dJointCreateContact(p.world, p.contactgroup, &contact);
		dJointAttach(contactJoint, b1, b2);
	}
}
