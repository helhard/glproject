module camera;

import gl3n.linalg;
import gl3n.math;
import std.experimental.logger;
import std.algorithm.comparison: clamp;

interface Camera {
	mat4 getView();
	mat4 getProjection(int, int);
}

class FPSCamera : Camera {
	vec3 position = vec3(0,0,0);
	vec3 up = vec3(0,1,0);
	float yaw = 0.0;
	float pitch = 0.0;

	float yawSpeed = 60.0;
	float pitchSpeed = 60.0;
	float maxPitch = 89.0;

	float fov = 70.0;
	float near = 0.1;
	float far = 1000.0;

	this(vec3 position, vec3 direction) {
		this.position = position;
		direction.normalize;
		this.pitch = asin(direction.y);
		this.yaw = atan2(-direction.x, -direction.z);
	}
	
	//void update(vec3 position, float dt, float vertical, float horizontal) {
	//	this.position = position;
	//	pitch = pitch + vertical * pitchSpeed * dt;
	//	yaw = yaw + horizontal * yawSpeed * dt;
	//}

	mat4 getView() {
		float cosPitch = cos(pitch);
		float sinPitch = sin(pitch);

		float cosYaw = cos(yaw);
		float sinYaw = sin(yaw);

		auto xaxis = vec3(cosYaw, 0, -sinYaw);
		auto yaxis = vec3(sinYaw * sinPitch, cosPitch, cosYaw * sinPitch);
		auto zaxis = vec3(sinYaw * cosPitch, -sinPitch, cosPitch * cosYaw);

		mat4 view = mat4.identity;
		view.matrix[0][0..3] = xaxis.vector;
		view.matrix[1][0..3] = yaxis.vector;
		view.matrix[2][0..3] = zaxis.vector;

		view.matrix[0][3] = -dot(xaxis, position);
		view.matrix[1][3] = -dot(yaxis, position);
		view.matrix[2][3] = -dot(zaxis, position);

		return view;
	}

	mat4 getProjection(int width, int height) {
		return mat4.perspective(width, height, fov, near, far);
	}
}
