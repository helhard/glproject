module game_manager;

import std.experimental.logger;

import camera;
import components;
import draw_system;
import gl3n.linalg;
import gl3n.math;
import inputs;
import ode_physics;
import sdlang;
import window_manager;
import asset_loader;
import std.json;
import std.traits;
import std.conv;
import entity_manager;


/**
 	GameManager
 */
class GameManager
{
	FPSCamera camera;
	AssetLoader loader;
	DrawSystem drawSystem;
	OdePhysics physicsSystem;

	EntityManager entityManager;


	this()
	{
		camera = new FPSCamera(vec3(0), vec3(0, 0, -1));
		drawSystem = new DrawSystem();
		physicsSystem = new OdePhysics();
		loader = new AssetLoader();
	}

	void update(float dt, WindowManager window, Inputs inputs)
	{
		physicsSystem.update(dt);
		updateDrawSystem(dt, window);
	}

	void updateDrawSystem(float dt, WindowManager window)
	{
		auto view = camera.getView();
		auto projection = camera.getProjection(window.width, window.height);

		drawSystem.draw(view, projection);

		//foreach(id; entities) {
		//	if(maskMatches(id, ComponentType.Position, ComponentType.Model)) {
		//		auto meshes = models[id].meshes;
		//		auto position = positions[id].position;
		//		drawSystem.drawMeshes(meshes, position, view, projection);
		//	}
		//}
	}
}
